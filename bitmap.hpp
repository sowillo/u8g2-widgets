/*
 * bitmap.hpp
 *
 *  Created on: Aug 17, 2017
 *      Author: fAX
 */

#ifndef BITMAP_HPP_
#define BITMAP_HPP_

#include "common.hpp"

namespace u8g2 {

   class XBM {
   public:
      XBM( );
      virtual ~XBM( );

      unsigned const char* data;
      dim width;
      dim height;
   };

} /* namespace u8g2 */

#endif /* BITMAP_HPP_ */

#include "bitmap.hpp"

namespace u8g2 {

   XBM::XBM( unsigned char *data, dim width, dim height ) {
      // TODO Auto-generated constructor stub

   }

   XBM::~XBM( ) {
      // TODO Auto-generated destructor stub
   }

} /* namespace u8g2 */
