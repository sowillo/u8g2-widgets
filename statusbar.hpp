/*
 * statusbar.hpp
 *
 *  Created on: Aug 17, 2017
 *      Author: fAX
 */

#ifndef STATUSBAR_HPP_
#define STATUSBAR_HPP_

#include "widget.hpp"

namespace u8g2 {

   class StatusBar: public Widget {
   public:
      StatusBar( );
      virtual ~StatusBar( );



   };

} /* namespace u8g2 */

#endif /* STATUSBAR_HPP_ */
