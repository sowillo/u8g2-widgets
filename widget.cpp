/*
 * widget.cpp
 *
 *  Created on: Aug 17, 2017
 *      Author: fAX
 */

#include "widget.hpp"

namespace u8g2 {

   Widget::Widget( ) {
      // TODO Auto-generated constructor stub

   }

   Widget::~Widget( ) {
      // TODO Auto-generated destructor stub
   }

   time_delta Widget::draw( ) {
      time_delta next_update = limit< time_delta > ::max_value;
      for ( auto c: children() ) {
         next_update = std::min( next_update, c->draw() )
      }
   }

      time_delta Widget::next_redraw_time( ) {
      return time_delta( 0 );
   }

} /* namespace u8g2 */
