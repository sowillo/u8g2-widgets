/*
 * debug.hpp
 *
 *  Created on: Aug 18, 2017
 *      Author: fAX
 */

#ifndef DEBUG_HPP_
#define DEBUG_HPP_


#define ERROR( ... )
#define WARN( ... )
#define INFO( ... )


#endif /* DEBUG_HPP_ */
