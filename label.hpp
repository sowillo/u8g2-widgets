/*
 * label.h
 *
 *  Created on: Aug 17, 2017
 *      Author: fAX
 */

#ifndef LABEL_HPP_
#define LABEL_HPP_

#include "widget.hpp"
#include "bitmap.hpp"

#include <vector>

namespace u8g2 {

   class Icon: public Widget {
   public:
      Icon( );
      virtual ~Icon( );

      void set_xpm( const XBM *bmp );
   };

   class MultiIcon: public Icon {
   public:

      uint8_t add_frame( const XBM *bmp );

      void set_frame( uint8_t idx );


   private:
      std::vector< XBM* > m_frames;

   };

} /* namespace u8g2 */

#endif /* LABEL_HPP_ */
