/*
 * widget.hpp
 *
 *  Created on: Aug 17, 2017
 *      Author: fAX
 */

#ifndef WIDGET_HPP_
#define WIDGET_HPP_

#include "common.hpp"

namespace u8g2 {

   class Layout;

   class Size {
      dim width;
      dim height;
   };

   /**
    * Widget is the basic display unit.
    *
    * Widget can contain children (@see children)
    *
    * Widget can be animated. The draw() function returns the next time it should be called
    */

   class Widget {
   public:
      Widget( );
      virtual ~Widget( );

      Size size() const;

      void add_child( Widget* child );
      void set_layout( Layout* layout );

      /** Marks this widget (and all its parents) as updated */
      void set_updated() { m_updated = true; if (m_parent) m_parent -> set_updated(); }

      /**
       *
       * @return next redraw time of the widget
       */
      virtual time_delta next_redraw_time();

      /**
       * Draws the widget on the screen
       *
       * @param force redrawing
       *
       * @return returns the next time when the widget wants to be redrawn
       */
      virtual time_delta draw( bool force = false );

   protected:
      dim m_size;
      bool m_updated;

      Widget* m_parent;
   };

} /* namespace u8g2 */

#endif /* WIDGET_HPP_ */
