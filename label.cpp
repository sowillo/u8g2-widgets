/*
 * label.cpp
 *
 *  Created on: Aug 17, 2017
 *      Author: fAX
 */

#include "label.hpp"

#include "debug.hpp"

namespace u8g2 {



} /* namespace u8g2 */

uint8_t u8g2::MultiIcon::add_frame( const XBM* bmp ) {
   m_frames.push_back( bmp );
   return m_frames.size() - 1;
}

void u8g2::MultiIcon::set_frame( uint8_t idx ) {
   if ( idx >= m_frames.size() ) {
      ERROR( "Frame index %d is invalid, only %d frames", idx, m_frames.size() );
      return;
   }

   if ( idx == m_current_index )
      return;

   set_xpm( frames[idx] );
   m_current_index = idx;

}

void u8g2::Icon::set_xpm( const XBM* bmp ) {

   set_updated();
}
