/*
 * common.hpp
 *
 *  Created on: Aug 17, 2017
 *      Author: fAX
 */

#ifndef COMMON_HPP_
#define COMMON_HPP_

#define F( x ) x
#define FS( x ) ustring( x )

namespace u8g2 {

   //! Widget dimensions type
   typedef uint8_t dim;

   //! Widget string type
   typedef std::string ustring;

   //! Animation delta time type
   typedef uint16_t time_delta;

   //! Absolute time type
   typedef uint32_t time;



} /* namespace u8g2 */

#endif /* COMMON_HPP_ */
